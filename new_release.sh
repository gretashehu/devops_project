#!/bin/bash


main_commit=$(git rev-parse --short main )
develop_commit=$(git rev-parse --short develop)


if git merge-base --is-ancestor $develop_commit $main_commit; then
   
  release_nr=$(sed -n 's/^App_version : [0-9]*\.[0-9]*\.//p' Release.txt)
  updated_release_no=$(($release_nr+1))

    
  sed -i "s/App_version :.*/App_version : 1.0.$updated_release_no/" Release.txt
  sed -i "s/Latest_commit: .*/Latest_commit: $main_commit/" Release.txt

    
  git add Release.txt
  git commit -m "Realease Number is: App_version : 1.0.$updated_release_no and updated latest commit hash is $(git rev-parse --short HEAD)"
  git push 

else

  echo "There are no merges."

fi

