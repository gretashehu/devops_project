#!/bin/bash
latest_commit=$(git rev-parse HEAD) #Store latest local commit
git pull origin main #Pull latest repository state

#Compare hash of latest local commit to latest repo state
if [[ "$latest_commit" == "$(git rev-parse origin/main)" ]]; then #Compare hash of latest local commit to latest repo state
  echo "Script exited, no differences found."
  exit 0

else

  latest_commit_short_hash=$(git rev-parse --short HEAD) #Store 7 first digits of the hash
  docker build -t gretashehu/reactapp:$latest_commit_short_hash /root/devops_project #Build docker image tagged with hash
  docker push gretashehu/reactapp:$latest_commit_short_hash #Push docker image tagged with hash

  #Upgrade the helm chart with the new image
  helm upgrade devops ./helm_chart  --set deployment.containers.tag="${latest_commit_short_hash}" 
  echo "Chart is upgraded, script finished."

fi